TARGET_DIR=$1
yarn generateMochawesome
gcloud auth activate-service-account --key-file=production-Appengine-ServiceAccount.json
gsutil -m cp -r testResults gs://fe-test-results/${TARGET_DIR}