const util = require('util');
const events = require('events');
const request = require('sync-request');
const { spawnSync } = require('child_process');
const replace = require('replace-in-file');

SlackReporter = function(baseReporter, config, options) {
  let passes = 0;
  let failures = 0;
  const failedTests = [];

  if (!process.env.CI) {
    return;
  }

  this.on('test:pass', function(test) {
    passes++;
  });

  this.on('test:fail', function(test, err) {
    failures++;
    failedTests.push(test);
  });

  this.on('end', function(runner) {
    const attachments = [];
    const fields = [];
    fields.push({
      title: options.message,
      value:
        passes + ' of ' + (passes + failedTests.length) + ' tests have passed',
      short: false
    });

    attachments.push({
      fallback: options.message,
      pretext: 'Finished Frontend Automated Test',
      color: 'good',
      fields: fields
    });

    if (failedTests.length > 0) {
      const fields = [];

      failedTests.forEach(function(failedTest) {
        fields.push({
          title: failedTest.title,
          value: failedTest.err.message,
          short: false
        });
      });

      attachments.push({
        fallback: options.message,
        title: 'Failed Tests',
        color: 'danger',
        fields: fields
      });
    }
    const targetReport = +new Date();
    const changes = replace.sync({
      files: './testResults/testResults.json',
      from: /testResults\//g,
      to: ''
    });

    console.log('Screenshot fixed: ', changes.toString());

    const upload = spawnSync('sh', [__dirname + '/upload_report.sh', targetReport]);
    
    console.log('Upload', upload.output.toString());

    attachments.push({
      fallback: 'Test Report',
      title: 'Test Report',
      text: 'Check test report for more detail',
      title_link: `https://storage.googleapis.com/fe-test-results/${targetReport}/testResults.html`,
      footer: '=========='
    });

    const res = request('POST', options.slackHook, {
      json: {
        channel: options.channel,
        username: options.username,
        attachments: attachments
      }
    });
  });
};

SlackReporter.reporterName = 'SlackReporter';

util.inherits(SlackReporter, events.EventEmitter);

exports = module.exports = SlackReporter;
